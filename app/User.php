<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Auth;
class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    
    /*
     * List user's post
     */
    public function posts(){
        return $this->hasMany('App\Post', 'user_id', 'id');
    }
    
    /*
     * Sign In by email
     */
    public static function signIn($data) {
        $result = array();
        $remember = false;
        if (array_key_exists('remember', $data)) {
            $remember = true;
        }
        if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']], $remember)) {
            $result['status'] = true;
            $result['message'] = 'Sign in successfully.';
        } else {
            $result['status'] = false;
            $result['message'] = 'Email or Password was wrong.';
        }
        return $result;
    }
    
    /*
     * Sign up an account
     */
    public static function store($data) {
        $user = new User();
        $data['password'] = bcrypt($data['password']);
        $user->fill($data);
        if ($user->save()) {
            $result['status'] = true;
            $result['message'] = 'Sign up successfully.';
            $result['user'] = $user;
        } else {
            $result['status'] = false;
            $result['message'] = 'Sign up was wrong.';
        }
        return $result;
    }
}
