<?php
namespace App\Helpers;


class Util
{
    public static $status_post = [
            'public'=>'Public',
            'private'=>'Private',
        ];
    
    public static function alert($result) {
        $alert = array();
        if ($result['status'] == false)
            $alert['class'] = 'alert-danger';
        else
            $alert['class'] = 'alert-success';

        $alert['message'] = $result['message'];
        session()->flash('alert', $alert);
    }
    
    public static function redirect($status, $back, $next,  $withInput= true, $flag='url'){
        $redirect = null;
        if($flag==='route'){
            if($status){
                $redirect = redirect()->route($next);
            }
            else{
                $redirect = redirect()->route($back);
            }
        }
        else if($flag==='url'){
            if($status){
                $redirect = redirect($next);
            }
            else{
                $redirect = redirect($back);
            }
        }
        
        if($withInput && !$status){
            $redirect = $redirect->withInput();
        }
        
        return $redirect;
    }
}