<?php
namespace App\Http\ViewComposers;
use Illuminate\Contracts\View\View;
use Auth;
class GlobalComposer{
	public function compose( View $view )
	{
		$isLogin = false;
        $user = null;
        if(Auth::check()) {
            $isLogin = true;
            $user = Auth::user();
        }
	    $view->with('isLogin', $isLogin);
	    $view->with('userAuth', $user);
	}
}