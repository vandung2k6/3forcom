<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['uses'=>'UserController@posts']);
Route::get('users/{id}/posts', ['uses'=>'UserController@userPosts']);

Route::group(['middleware' => 'guest'], function () {
    /*
     * Sign in form
     */
    Route::get('sign-in', 
        ['as'=>'signIn', 'uses'=>'AuthController@signIn']
    );
    
    /*
     * Sign in handle
     */
    Route::post('sign-in', 
        ['as'=>'postSignIn', 'uses'=>'AuthController@postSignIn']
    );
    
    /*
     * Sign up form
     */
    Route::get('sign-up', 
        ['as'=>'signUp', 'uses'=>'AuthController@signUp']
    );
    
    /*
     * Sign up handle
     */
    Route::post('sign-up', 
        ['as'=>'postSignUp', 'uses'=>'AuthController@postSignUp']
    );
});

Route::group(['middleware' => 'auth'], function () {
    /*
     * posts
     */
    Route::resource('post', 'PostController');
    
    /*
     * Sign out handle
     */
    Route::get('sign-out', 
        ['as'=>'signOut', 'uses'=>'AuthController@signOut']
    );
});