<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Post;
use App\User;

class UserController extends Controller
{
    /*
     * All post 
     */
    public function posts(){
        $posts = Post::all();
        $users = User::all();
        return view('user.post_all')
                ->with('posts', $posts)
                ->with('users', $users);
    }
    
    public function userPosts($id){
        $user = User::find($id);
        $posts = $user->posts()->get();
        return view('user.user_posts')
                ->with('posts', $posts)
                ->with('user', $user);
    }
}
