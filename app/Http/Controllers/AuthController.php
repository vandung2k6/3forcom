<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestSignIn;
use App\Http\Requests\RequestSignUp;
use App\User;
use App\Helpers\Util;
use Auth;
class AuthController extends Controller
{
    private $redirect = 'post';
    
    /*
     * Sign in form
     */
    public function signIn(){
        return view('auth.sign_in');
    }
    
    /*
     * handle sign in
     */
    public function postSignIn(RequestSignIn $request){
        $result = User::signIn($request->all());
        Util::alert($result);
        return Util::redirect($result['status'], 'sign-in', $this->redirect);
    }
    
    /*
     * Sign up form
     */
    public function signUp(){
        return view('auth.sign_up');
    }
    
    /*
     * Handle sign up
     */
    public function postSignup(RequestSignUp $request){
        $result = User::store($request->all());
        Util::alert($result);
        if($result['status']){
            Auth::login($result['user']);
        }
        return Util::redirect($result['status'], 'sign-up', 'sign-in');
    }
    
    public function signOut(){
        Auth::logout();
        session()->flush();
        return redirect('/');
    }
}
