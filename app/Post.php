<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'title', 'content', 'status'];
    
    /**
     * Get the user of post.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'id', 'user_id');
    }
    
    /*
     * Store
     */
    public function store($data){
        $this->fill($data);
        if ($this->save()) {
            $result['status'] = true;
            $result['message'] = 'Add post successfully.';
        } else {
            $result['status'] = false;
            $result['message'] = 'Add post was wrong.';
        }
        return $result;
    }
}
