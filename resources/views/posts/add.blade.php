@extends('layouts.default')
@section('style')

@stop

@section('content')
<div class="container">
    <div id="side_box">
        <h3>Add a post</h3>
        <hr />
        {!! Form::open( array('url' => 'post', 'id'=>'frm_add_post', 'class'=>'form-horizontal' ) ) !!}
        <div class="form-group">
            <label for="" class="col-sm-12">Title:</label>
            <div class="col-sm-12">
              {!! Form::text('title', null, array('class' => 'form-control','id'=>'title','autofocus', 'placeholder'=>'Title')) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-12">Content:</label>
            <div class="col-sm-12">
              {!! Form::textarea('content', null, ['class'=>'form-control', 'id'=>'post_content', 'autocomplete'=>'off', 'rows'=>'5', 'placeholder'=>""]) !!}
              <label id="post_content-error" class="error" for="post_content"></label>
            </div>
        </div>
        
        <div class="form-group">
            <label for="" class="col-sm-12">Status:</label>
            <div class="col-sm-12">
              {!! Form::select('status', [null=>'---'] + \App\Helpers\Util::$status_post, null, ['class'=>'form-control']) !!}
            </div>
        </div>
        
         <div class="form-group">
            <div class="col-sm-12">
                <br />
              {!! Form::button('Save', ['class'=>'btn btn-primary', 'id'=>'btn_save']) !!}
              &nbsp;&nbsp;<a href="{{ url('post') }}">Cancel</a>
            </div>
        </div>
        {!! Form::hidden('user_id', $userAuth->id) !!}
        {!! Form::close () !!}
    </div>
</div>
@stop

@section('script')
<!-- jquery validate-->
<script type="text/javascript" src="{{ asset('plugins/validate/js/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/validate/js/jquery.validate.min.js') }}"></script>

<!--Ck Editor-->
<script src="//cdn.ckeditor.com/4.5.4/basic/ckeditor.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    /*
     * Add ckeditor
     */
    CKEDITOR.replace( 'post_content' );
    
    /*
     * Validate form 
     */
    $("#frm_add_post").validate({
        ignore:[],
        rules: {
          title: {required: true, minlength:10},
          content: { required: true, minlength: 10 },
          status:{ required: true}
        },
        submitHandler: function(form) {
          $('#box-processing').show();
          form.submit();
        }
    });
    
    $('#btn_save').click(function(){
        CKEDITOR.instances['post_content'].updateElement();
        $("#frm_add_post").submit();
    });
});
</script>
@stop