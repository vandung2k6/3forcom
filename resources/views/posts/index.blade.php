@extends('layouts.default')
@section('style')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap.css') }}">
@stop

@section('content')
<div class="container">
    <div id="side_box">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">List posts 
                  <a class="pull-right" href="{{ url('post/create') }}"> <span class="glyphicon glyphicon-plus"></span>Add Post</a>
              </h3>
            </div><!-- /.box-header -->
            <hr />
            <div class="box-body">
                <table id="posts" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Created At</th>
                    <th>Functions</th>
                  </tr>
                </thead>
                <tbody>
                    <?php $stt = 1; ?>
                    @foreach($posts as $rs)
                        <tr>
                            <td>{{ $stt }}</td>
                            <td>{{ $rs['id'] }}</td>
                            <td>{{ $rs['title'] }}</td>
                            <td>{{ $rs['status'] }}</td>
                            <td>{{ $rs['created_at'] }}</td>
                            <td>
                                {!! Form::open( array('method'=>'DELETE', 'url' => 'post/'.$rs->id, 'class'=>'inner-line' ) ) !!}
                                    <a class="btn btn-warning" href="{{ url('post/'.$rs->id.'/edit') }}">Update</a>
                                    {!! Form::submit('Delete', array('class'=>'btn btn-danger')) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        <?php $stt++; ?>
                    @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Created At</th>
                    <th>Functions</th>
                  </tr>
                </tfoot>
              </table>
            </div>
        </div>
    </div>
</div>
@stop

@section('script')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<script type="text/javascript">
$(document).ready(function(){
    $('#posts').DataTable();
});
</script>
@stop