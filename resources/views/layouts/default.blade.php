@include('layouts.header')

@section('nav_bar')
<header>
<nav class="navbar navbar-dzung">
    <div class="container">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
              <a class="navbar-brand" href="{{ url('/') }}">
                  <img alt="Dung" src="{{ asset('web/img/logo.png') }}" height="35">
              </a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
              @if($userAuth)
                <li><a href="{{ route('post.index') }}">My Post</a></li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ $userAuth->name }} <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="{{ route('signOut') }}">Log Out</a></li>
                  </ul>
                </li>
               @else
               <li><a href="{{ route('signIn') }}">Sign In</a></li>
               <li><a href="{{ route('signUp') }}">Sign Up</a></li>
                @endif
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </div>
</nav>
        
</header> <!--end header-->
@show

<div id="content">
    @include('layouts.alert')
    @yield('content')
</div>

@include('layouts.footer')