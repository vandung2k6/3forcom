<!--alert-->
@if(session()->has('alert'))
<div class="container row-space-top-2 row-space-2">
    <div class="alert alert-dismissable {{ session('alert')['class'] }}">
        {{ session('alert')['message'] }}
    </div>
</div>
@endif

@if($errors->any())
<div class="container row-space-top-2 row-space-2">
    <ul class="alert alert-danger list-unstyled">
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<!--end alert-->