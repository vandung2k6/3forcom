<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @yield('title')

        <!-- Bootstrap -->
        <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('bootstrap/css/bootstrap-theme.min.css') }}" rel="stylesheet">
        <!--end bootstrap-->
        
        <!--style site-->
        <link href="{{ asset('web/css/style.css') }}" rel="stylesheet">

        @yield('style')
    </head>
    <body>