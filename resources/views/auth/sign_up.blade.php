@extends('layouts.default')

@section('style')
<link href="{{ asset('web/css/auth.css') }}" rel="stylesheet">
@stop

@section('content')

<div class="container">
        <div class="row-space-2 row-space-top-2">
          <div id="box-sign-in">
            <h2 class="row-space-1 text-center">Sign Up</h2>
            <hr/>
            {!! Form::open( array('url' => 'sign-up', 'id'=>'frm_signup', 'class'=>'form-horizontal' ) ) !!}
                <div class="form-group">
                  <div class="col-sm-12">
                    {!! Form::text('name', null, array('class' => 'form-control', 'placeholder'=>'Full name')) !!}
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    {!! Form::text('email', null, array('class' => 'form-control', 'placeholder'=>'Email')) !!}
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                      {!! Form::password('password', array('class' => 'form-control','id' => 'password', 'placeholder'=>'Password')) !!}
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                      {!! Form::password('confirm_password', array('class' => 'form-control', 'placeholder'=>'Confirm password')) !!}
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <button type="button" class="btn btn-block btn-dung1" id="btn_signup_submmit">Submit</button>
                  </div>
                </div>
            {!! Form::close () !!}
            <hr>
            <div class="form-group">
              <div class="col-sm-12">
                Have an account? <a class="dung" href="{{ route('signIn') }}">Sign In</a>
              </div>
            </div>
          </div>
      </div>
    </div>
@stop

@section('script')
<!-- jquery validate-->
<script type="text/javascript" src="{{ asset('plugins/validate/js/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/validate/js/jquery.validate.min.js') }}"></script>

<script type="text/javascript">
$(document).ready(function(){
    /*
     * Validate form 
     */
    $("#frm_signup").validate({
        rules: {
          name: { required: true,minlength: 2 },
          email: {required: true, email:true},
          password: { required: true, minlength: 6 },
          confirm_password: { required: true, minlength: 6, equalTo: "#password" }
        },
        submitHandler: function(form) {
          $('#box-processing').show();
          form.submit();
        }
    });
    
    
    /*
     * Click submit form
     */
    $('#btn_signup_submmit').click(function(){
      $("#frm_signup").submit();
    });
});
</script>
@stop