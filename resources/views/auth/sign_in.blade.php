@extends('layouts.default')

@section('style')
<link href="{{ asset('web/css/auth.css') }}" rel="stylesheet">
@stop

@section('content')
<div class="container">
    <div id="box-sign-in">
        <h2 class="text-center">Sign In</h2>
        <div class="clear"></div>
        <hr/>
        {!! Form::open( array('url' => 'sign-in', 'id'=>'frm_signin', 'class'=>'form-horizontal' ) ) !!}
          <div class="form-group">
            <div class="col-sm-12">
              {!! Form::text('email', null, array('class' => 'form-control','id'=>'email','autofocus', 'placeholder'=>'Email')) !!}
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-12">
                {!! Form::password('password', array('class' => 'form-control','id' => 'password', 'placeholder'=>'Password')) !!}
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-12">
                <div class="checkbox">
                  <label>
                    {!! Form::checkbox('remember', 'on') !!} Remember
                  </label>
                </div>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-12">
              <button type="button" class="btn btn-block btn-dung1" id="btn_signin_submmit">Submit</button>
            </div>
          </div>
        {!! Form::close () !!}
        <hr>
        <div class="form-group">
          <div class="col-sm-12">
            haven't an account? <a class="dung" href="{{ route('signUp') }}">Sign Up</a>
          </div>
        </div>
      </div>
</div>
@stop

@section('script')
<!-- jquery validate-->
<script type="text/javascript" src="{{ asset('plugins/validate/js/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/validate/js/jquery.validate.min.js') }}"></script>

<script type="text/javascript">
$(document).ready(function(){
    /*
     * Validate form 
     */
    $("#frm_signin").validate({
        rules: {
          email: {required: true, email:true},
          password: { required: true, minlength: 6 },
        },
        submitHandler: function(form) {
          $('#box-processing').show();
          form.submit();
        }
    });
    
    /*
     * Submit form
     */
    $('#btn_signin_submmit').click(function(){
      $("#frm_signin").submit();
    });
});
</script>
@stop