@extends('layouts.default')
@section('style')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap.css') }}">
@stop

@section('content')
<div class="container">
    <div id="side_box">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ $user->name }}'s posts </h3>
            </div><!-- /.box-header -->
            <hr />
            <div class="box-body">
                <table id="posts" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Created At</th>
                  </tr>
                </thead>
                <tbody>
                    <?php $stt = 1; ?>
                    @foreach($posts as $rs)
                        <tr>
                            <td>{{ $stt }}</td>
                            <td>{{ $rs['id'] }}</td>
                            <td>{{ $rs['title'] }}</td>
                            <td>{{ $rs['status'] }}</td>
                            <td>{{ $rs['created_at'] }}</td>
                        </tr>
                        <?php $stt++; ?>
                    @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Created At</th>
                  </tr>
                </tfoot>
              </table>
            </div>
        </div>
    </div>
</div>
@stop

@section('script')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<script type="text/javascript">
$(document).ready(function(){
    $('#posts').DataTable();
});
</script>
@stop