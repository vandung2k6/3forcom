@extends('layouts.default')
@section('style')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap.css') }}">
@stop

@section('content')
<div class="container">
    <div id="side_box">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">List posts</h3>
            </div><!-- /.box-header -->
            <hr />
            <div class="box-body">
                <table id="posts" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Created At</th>
                  </tr>
                </thead>
                <tbody>
                    <?php $stt = 1; ?>
                    @foreach($posts as $rs)
                        <tr>
                            <td>{{ $stt }}</td>
                            <td>{{ $rs['id'] }}</td>
                            <td>{{ $rs['title'] }}</td>
                            <td>{{ $rs['status'] }}</td>
                            <td>{{ $rs['created_at'] }}</td>
                        </tr>
                        <?php $stt++; ?>
                    @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Created At</th>
                  </tr>
                </tfoot>
              </table>
            </div>
        </div>
        
        <hr />
        
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">List Users</h3>
            </div><!-- /.box-header -->
            <hr />
            <div class="box-body">
                <table id="users" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Created At</th>
                    <th>Post</th>
                  </tr>
                </thead>
                <tbody>
                    <?php $stt = 1; ?>
                    @foreach($users as $rs)
                        <tr>
                            <td>{{ $stt }}</td>
                            <td>{{ $rs['name'] }}</td>
                            <td>{{ $rs['email'] }}</td>
                            <td>{{ $rs['created_at'] }}</td>
                            <td><a href="{{ url('users/'.$rs->id.'/posts') }}">Posts</a></td>
                        </tr>
                        <?php $stt++; ?>
                    @endforeach
                </tbody>
                <tfoot>
                  <tr>
                     <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Created At</th>
                    <th>Post</th>
                  </tr>
                </tfoot>
              </table>
            </div>
        </div>
    </div>
</div>
@stop

@section('script')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<script type="text/javascript">
$(document).ready(function(){
    $('#posts').DataTable();
    $('#users').DataTable();
});
</script>
@stop